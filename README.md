# Trading Strategies

Implementation of Trading Strategies for Backtesting in MetaTrader 5

| Expert Advisor | Strategy Video | Description |
| --- | :---: | --- |
| [CEZLSMA](Experts/CEZLSMA.mq5) | [:arrow_forward:](https://youtu.be/2U5VTWBBK8U) | A strategy using [Chandelier Exit](Indicators/ChandelierExit.mq5) and [ZLSMA](Indicators/ZLSMA.mq5) indicators based on the Heikin Ashi candles |
| [3MAF](Experts/3MAF.mq5) | [:arrow_forward:](https://youtu.be/bKPs2aOsvsk) | A strategy using three Moving Averages and Williams Fractals for scalping |
| [BBRSI](Experts/BBRSI.mq5) | [:arrow_forward:](https://youtu.be/pCmJ8wsAS_w) | A strategy using Bollinger Bands and RSI |
| [DHLAOS](Experts/DHLAOS.mq5) | [:arrow_forward:](https://youtu.be/IZVSb1kjduQ) | A strategy using Daily High/Low and Andean Oscillator indicators for scalping |


Additionally, there are strategies available for utilization within TradingView. If this piques your interest, you can explore them in this [repository](https://github.com/geraked/tradingview).

## Author

**Rabist** - view on [LinkedIn](https://www.linkedin.com/in/rabist)

## License

Licensed under [MIT](LICENSE).
